REGISTRY_NAME=registry.gitlab.com
IMAGE_NAME=nathamanath/blog
VERSION=$(shell cat ./version.txt)
ENVIRONMENT=production

release: build docker

docker: docker.build docker.push

docker.build:
	docker build --build-arg RUBY_VERSION=2.4.3 --build-arg RACK_ENV=${ENVIRONMENT} -t ${IMAGE_NAME} .
	docker tag  ${IMAGE_NAME} ${REGISTRY_NAME}/${IMAGE_NAME}:${VERSION}

docker.push:
	docker push ${REGISTRY_NAME}/${IMAGE_NAME}:${VERSION}

install:
	cd ./ui && npm install

build:
	cd ui && npm install
	cp ./static/* ./public/
	cd ./ui && npm run prod

PHONY: docker build docker.build docker.push
